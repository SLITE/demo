import { Component, OnInit} from '@angular/core';

import { User } from './user';
import { UserService } from './../services/user.service';
import { MessageService } from './../services/message.service';

@Component({
  selector: 'user-part',
  templateUrl: './user.html',
})
export class UserComponent implements OnInit  { 
	users: User[]; 
  user: User;
  pattern: string;
  currentPage: number;
  totalPage: number;
  pageSize: number;
  pagination: number[];
	constructor(
    private userService: UserService,
    private messageService: MessageService) { }

    search(): void {
      this.userService.search(this.pattern,this.currentPage).then(users => {this.users = users; console.log("search");console.log(users);});  
    }


    clearName(): void {
        this.messageService.clearMessage();
    }

    name(): void {
      this.messageService.sendMessage('Dmitrii !!');
    }

    getUsers(): void {
      this.userService.getUsers().then(users => {this.users = users; console.log(users);});
    }

    nextPage(page:number): void {
      this.currentPage = page;  
      this.search();
    }

    getCountPage(): void {
      this.userService.getCountPage().then(count => {
        let res : any =  count;
        this.totalPage = Math.ceil(res/this.pageSize); 
        //this.pagination = Array(this.totalPage).fill().map((value:number,i:number,arr:any[])=>i);

        console.log(this.totalPage);});
    }

    ngOnInit(): void {
      this.pageSize = 3;
      this.user = new User();
      this.users = [];
      this.currentPage = 0;
      this.pattern = '';
      this.totalPage = 0;
      this.pagination = [];
    	this.getUsers();
      this.getCountPage();
  	}
}
