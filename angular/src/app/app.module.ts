import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule }   from '@angular/router';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms';

import { AppComponent }  from './app.component';

import { UserService }  from './services/user.service';
import { MessageService }  from './services/message.service';
import { UserComponent }  from './user/user.component';
import { ArticleComponent }  from './article/article.component';

@NgModule({
  imports:      [ BrowserModule,
  					FormsModule,
  					HttpModule,
				  RouterModule.forRoot([
				      {
				        path: 'user',
				        component: UserComponent
				      },
				      {
				        path: 'article',
				        component: ArticleComponent
				      },
				    ]) ],
  declarations: [ AppComponent, UserComponent,ArticleComponent],
  providers: [ UserService, MessageService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
