package demo.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import demo.managers.UserManager;


import demo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by dmsh0714 on 06.07.2016.
 */
@Controller
public class UserController {
    @Autowired
    private UserManager userManager;

    @RequestMapping("/api/initUser")
    @ResponseBody
    public String initUsers()  {
        List<String> names = new ArrayList<String>(){
            {
               add("Dmitrii");
               add("Ivan");
               add("Nikolai");
               add("Igor");
               add("Kirill");
               add("Olga");
               add("Anastasy");
            }
        };
        try {
            for(int i=0;i<10;i++){
                User user = new User();
                Random rnd = new Random();
                int j = rnd.nextInt(names.size());
                user.setName(names.get(j));
                user.setAge(ThreadLocalRandom.current().nextInt(10, 88));
                user.setJobTitle("Junior");
                userManager.create(user);
            }
        }
        catch (Exception ex) {
            return "Error creating the user: " + ex.toString();
        }
        return "User succesfully created!";
    }

    @RequestMapping("/api/createUser")
    @ResponseBody
    public String create(@RequestBody User userModel)  {
        try {
            /*User user = new User();
            user.setName(userModel.getName());
            user.setName(userUI.getName());
            user.setPosition(positionsManager.getById(userUI.getPositionId()));
            user.setPassword(userUI.getPassword());*/
            userManager.create(userModel);
        }
        catch (Exception ex) {
            return "Error creating the user: " + ex.toString();
        }
        return "User succesfully created!";
    }


    @RequestMapping("/api/updateUser")
    @ResponseBody
    public String update(User user) {
        try {
            userManager.create(user);
        }
        catch (Exception ex) {
            return "Error updating the user: " + ex.toString();
        }
        return "User succesfully updated!";
    }

    @RequestMapping(value = "/api/getUser/{id}",method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User> getById(@PathVariable int id)  {
        User user;
        try {
             user = userManager.getUserById(id);
        }
        catch (Exception ex) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user,HttpStatus.OK);
    }


    @RequestMapping("/api/getAllUsers")
    @ResponseBody
    public List<User> getAllUsers() {
        List<User> users;
        try {
            users = userManager.getUsers();
        }
        catch (Exception ex) {
            return Collections.emptyList();
        }
        return users;
    }

    @RequestMapping("/api/getUsersCount")
    @ResponseBody
    public Integer getUsers(){
        Integer users = userManager.getUsers().size();
        return users;
    }

    @RequestMapping("/api/getUsers")
    @ResponseBody
    public List<User> getUsers(@RequestParam(value = "search") String pattern,
                               @RequestParam(value = "page") int page){
        List<User> users;
        try {
            users = userManager.getUsers(page,"%"+pattern+"%");
        }
        catch (Exception ex) {
            return Collections.emptyList();
        }
        return users;
    }


    @RequestMapping("/api/delete")
    @ResponseBody
    public String delete(int id) {
        try {
            userManager.delete(id);
        }
        catch (Exception ex) {
            return "Error deleting the user: " + ex.toString();
        }
        return "User succesfully deleted!";
    }
}
