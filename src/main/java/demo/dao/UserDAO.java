package demo.dao;

import demo.models.User;

import java.util.List;

/**
 * Created by dmsh0714 on 05.04.2017.
 */
public interface UserDAO {
    void create(User user);
    void delete(User user);
    void update(User user);
    List<User> getUsers();
    List<User> getUsers(int page,String pattern);
    List<User> getUserByName(String name);
    User getUserById(int id);
}
