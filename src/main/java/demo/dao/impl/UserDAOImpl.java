package demo.dao.impl;

import demo.dao.UserDAO;
import demo.models.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by dmsh0714 on 05.04.2017.
 */
@Service
public class UserDAOImpl implements UserDAO {


    private static String FIELD_NAME = "name";

    private static int PAGE_SIZE = 3;

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void create(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
    }

    @Override
    public void delete(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(user);
    }

    @Override
    public void update(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }

    @Override
    public List<User> getUsers() {
        Session session = sessionFactory.getCurrentSession();
        List<User> users = session.createCriteria(User.class).list();
        return users;
    }

    @Override
    public List<User> getUsers(int page,String pattern) {
        Session session = sessionFactory.getCurrentSession();
        Criteria userList = session.createCriteria(User.class);
        if(pattern!=null && !pattern.isEmpty()){
            userList.add(Restrictions.like(FIELD_NAME, pattern));
        }
        userList.setFirstResult(page*PAGE_SIZE);
        userList.setMaxResults(PAGE_SIZE);
        return userList.list();
    }

    @Override
    public List<User> getUserByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Criteria usersCriteria = session.createCriteria(User.class);
        List<User> users = usersCriteria.add(Restrictions.like(FIELD_NAME, name)).list();
        return users;
    }

    @Override
    public User getUserById(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class,id);
        return user;
    }
}
