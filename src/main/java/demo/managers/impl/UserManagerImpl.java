package demo.managers.impl;

import demo.dao.UserDAO;
import demo.managers.UserManager;
import demo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * Created by dmsh0714 on 05.04.2017.
 */
@Service
public class UserManagerImpl implements UserManager {

    @Autowired
    private UserDAO userDao;

    @Transactional
    @Override
    public void create(User user) {
        userDao.create(user);
    }

    @Transactional
    @Override
    public void delete(int id) {
        User user = userDao.getUserById(id);
        userDao.delete(user);
    }

    @Transactional
    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Transactional
    @Override
    public List<User> getUsers() {
        return userDao.getUsers();
    }

    @Transactional
    @Override
    public List<User> getUsers(int page,String pattern) {
        return userDao.getUsers(page,pattern);
    }

    @Transactional
    @Override
    public List<User> getUserByName(String name) {
        return userDao.getUserByName(name);
    }

    @Transactional
    @Override
    public User getUserById(int id) {
        return userDao.getUserById(id);
    }
}
