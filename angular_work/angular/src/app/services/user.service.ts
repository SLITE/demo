import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { User } from './../user/user';

@Injectable()
export class UserService {
  private headers = new Headers({'Content-Type': 'application/json'});
  //private userUrl = '/api/getUsers'; 
  private countUserUrl = '/api/getUsersCount';
  private searchUserUrl = '/api/getUsers';
  private createUserUrl = '/api/createUser';
  private userUrl = '/api/getAllUsers';
  //private userUrl = '/user.json';
  //private searchUserUrl = '/user.json';
  //private countUserUrl = '/page.json';

  constructor(private http: Http) { }

  getUsers(): Promise<User[]> {
    return this.http.get(this.userUrl)
               .toPromise()
               .then(response => response.json() as User[])
               .catch(this.handleError);
  }	

  getCountPage(): Promise<Number> {
    return this.http.get(this.countUserUrl)
               .toPromise()
               .then(response => response.json() as Number)
               .catch(this.handleError);
  } 

  getUser(id: number): Promise<User> {
    const url = `${this.userUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }

  search(pattern: string, page: number): Promise<User[]> {
    console.log("Pat "+pattern+" page "+page);
    const url = `${this.searchUserUrl}?page=${page}&search=${pattern}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as User[])
      .catch(this.handleError);
  }
  
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
 }
}