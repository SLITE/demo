"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var user_1 = require("./user");
var user_service_1 = require("./../services/user.service");
var message_service_1 = require("./../services/message.service");
var UserComponent = (function () {
    function UserComponent(userService, messageService) {
        this.userService = userService;
        this.messageService = messageService;
    }
    UserComponent.prototype.search = function () {
        var _this = this;
        this.userService.search(this.pattern, this.currentPage).then(function (users) { _this.users = users; console.log("search"); console.log(users); });
    };
    UserComponent.prototype.clearName = function () {
        this.messageService.clearMessage();
    };
    UserComponent.prototype.name = function () {
        this.messageService.sendMessage('Dmitrii !!');
    };
    UserComponent.prototype.getUsers = function () {
        var _this = this;
        this.userService.getUsers().then(function (users) { _this.users = users; console.log(users); });
    };
    UserComponent.prototype.nextPage = function (page) {
        this.currentPage = page;
        this.search();
    };
    UserComponent.prototype.getCountPage = function () {
        var _this = this;
        this.userService.getCountPage().then(function (count) {
            _this.totalPage = Math.ceil(count / _this.pageSize);
            _this.pagination = Array(_this.totalPage).fill().map(function (x, i) { return i; });
            console.log(_this.totalPage);
        });
    };
    UserComponent.prototype.ngOnInit = function () {
        this.pageSize = 3;
        this.user = new user_1.User();
        this.users = [];
        this.currentPage = 0;
        this.pattern = '';
        this.totalPage = 0;
        this.pagination = [];
        this.getUsers();
        this.getCountPage();
    };
    return UserComponent;
}());
UserComponent = __decorate([
    core_1.Component({
        selector: 'user-part',
        templateUrl: './user.html',
    }),
    __metadata("design:paramtypes", [user_service_1.UserService,
        message_service_1.MessageService])
], UserComponent);
exports.UserComponent = UserComponent;
//# sourceMappingURL=user.component.js.map