import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { MessageService } from './services/message.service';

@Component({
  selector: 'my-app',
  templateUrl: './index.html',
})
export class AppComponent implements OnDestroy, OnInit { 
	message: string;
    subscription: Subscription;

	constructor(private messageService: MessageService) {
        this.subscription = this.messageService.getMessage().subscribe(message => { this.message = message!=null?message.text:""; });
    }

	ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    }
    ngOnInit(): void {
      this.message = 'Ivan';
  	}
}
