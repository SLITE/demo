import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'article',
  templateUrl: './article.html',
})
export class ArticleComponent implements OnInit  { 
  message: string;
  title: string;
	constructor() { }

  
    ngOnInit(): void {
      this.title = "Some title";
      this.message = "Some text Some text Some text Some text Some text Some text";
  	}
}
